using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class CameraFollow : MonoBehaviour
    {
        public bool _isFollow;

        public Transform _Playerhead;

        private Vector3 _BeginPos;

        // Update is called once per frame
        void LateUpdate()
        {
            FollowPlayer();
        }

        void FollowPlayer()
        {
            if (_Playerhead == null) return;
            if (!_isFollow) return;
            if (_Playerhead.transform.position.y < _BeginPos.y) return;
            this.transform.position = new Vector3(0, _Playerhead.transform.position.y, -10);
        }

        public void BeginSetup(bool _isCameraFollow, Vector3 _OriginalPosCamera, int CameraSize)
        {
            this._isFollow = _isCameraFollow;
            _BeginPos = _OriginalPosCamera;
            this.transform.position = _BeginPos;
            this.GetComponent<Camera>().fieldOfView = CameraSize;
            StrechCamera();
        }

        void StrechCamera()
        {
            float RatioCamera = (float)Screen.height / Screen.width;
            int DifCamSize = 0;
            float DiffCamPos = 0;
            if (RatioCamera < 2)
            {
                DifCamSize = 0;
                DiffCamPos = 0;
            }
            else if (RatioCamera < 2.1f)
            {
                DifCamSize = 7;
                DiffCamPos = 0.5f;
            }
            else
            {
                DifCamSize = 11;
                DiffCamPos = 1f;
            }

            this.GetComponent<Camera>().fieldOfView += DifCamSize;
            _BeginPos += new Vector3(0, DiffCamPos, 0);
            this.transform.position = _BeginPos;
        }
    }
}