﻿using System;
using UnityEngine;

namespace Gameplay.CharacterSystem
{
    public class CharacterHand : MonoBehaviour
    {
        enum HandState
        {
        }

        Rigidbody _rigidbody;
        Vector3 lastPosition;
        Vector3 currentPosition;
        public bool isHolding;
        Vector3 snapPoint;
        bool isSnaping;
        private Vector3 _orgiginalScale = new Vector3(0.5f, 0.5f, 0.5f);
        private Vector3 HoldingScaleSize = new Vector3(0.7f, 0.7f, 0.7f);

        [SerializeField] private LayerMask snapLayerMask;

        void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        void Update()
        {
            if (!isHolding) return;
            MoveHand();
            CheckSnapping();
        }

        void LateUpdate()
        {
            if (isSnaping)
            {
                _rigidbody.isKinematic = true;
                transform.position = snapPoint;
            }
        }

        void CheckSnapping()
        {
            var rayLeft = new Ray(currentPosition, Vector3.left);
            if (CheckSnapping(rayLeft))
            {
                //Debug.Log("Snap Left");

                return;
            }

            var rayRight = new Ray(currentPosition, Vector3.right);
            if (CheckSnapping(rayRight))
            {
                //Debug.Log("Snap Right");

                return;
            }

            var rayDown = new Ray(currentPosition, Vector3.down);
            if (CheckSnapping(rayDown))
            {
                //Debug.Log("Snap Down");

                return;
            }

            var rayUp = new Ray(currentPosition, Vector3.up);
            if (CheckSnapping(rayUp))
            {
                //Debug.Log("Snap Up");

                return;
            }
        }

        public void SnapToPoint(Vector3 point)
        {
            isSnaping = true;
            snapPoint = point;
        }

        bool CheckSnapping(Ray ray)
        {
            if (Physics.Raycast(ray, out var hit, 0.5f, snapLayerMask))
            {
                //Debug.Log(hit.point);
                isSnaping = true;
                snapPoint = hit.point;

                return true;
            }

            isSnaping = false;
            return false;
        }

        void MoveHand()
        {
            currentPosition = PlaneXY.gInstance.GetCurrentPosInPlane();
            transform.position = Vector3.Lerp(transform.position, currentPosition, 0.1f);
            // Vector3 posDelta = currentPosition - lastPosition;
            // lastPosition = currentPosition;
            // transform.position += posDelta;
        }

        void OnMouseDown()
        {
            //if (GameplayController._instance._State != GameplayController.GameState.Play) return;
            isHolding = true;
            lastPosition = PlaneXY.gInstance.GetCurrentPosInPlane();
            //Debug.Log(lastPosition);

            _rigidbody.isKinematic = true;
            this.transform.localScale = HoldingScaleSize;

            //Debug.Log("down");
        }

        void OnMouseUp()
        {
            isHolding = false;

            if (!isSnaping)
            {
                _rigidbody.isKinematic = false;
            }

            this.transform.localScale = _orgiginalScale;
            //Debug.Log("up");
        }
    }
}