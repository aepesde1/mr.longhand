using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.CharacterSystem
{
    public class CharacterLogicCheck : MonoBehaviour
    {
        private Player _Player;

        // Start is called before the first frame update
        void Start()
        {
            _Player = this.transform.parent.GetComponent<Player>();
        }

        // Update is called once per frame
        void Update()
        {
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag.Equals("Obstacle"))
            {
                _Player.OnBreak();
            }
        }
    }
}