using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;
using UnityEngine.Events;

namespace Gameplay.CharacterSystem
{
    public class HeadChecking : MonoBehaviour
    {
        public static HeadChecking _instance;

        [SerializeField] private Sprite FunFace;

        [SerializeField] private Sprite SadFace;

        [SerializeField] private Sprite WorryFace;

        [SerializeField] private Sprite DeadFace;

        [SerializeField] private SpriteRenderer _TheFace;

        public EmotionFace _currentEmotion;

        public EmotionFace CurrentEmotion
        {
            get { return _currentEmotion; }
            set
            {
                _currentEmotion = value;
                EmotionCheck();
            }
        }

        private void Awake()
        {
            if (_instance != null) Destroy(this.gameObject);
            else _instance = this;
        }

        private void Start()
        {
        }

        public void EmotionCheck()
        {
            switch (this._currentEmotion)
            {
                case EmotionFace.Fun:
                    _TheFace.sprite = FunFace;
                    break;
                case EmotionFace.Sad:
                    _TheFace.sprite = SadFace;
                    break;
                case EmotionFace.Worry:
                    _TheFace.sprite = WorryFace;
                    break;
                case EmotionFace.Dead:
                    _TheFace.sprite = DeadFace;
                    break;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Finish")
            {
                Mainmenu._instance.Victory();
                this._currentEmotion = EmotionFace.Fun;
            }
        }

        public enum EmotionFace
        {
            Fun = 1,
            Sad = 2,
            Worry = 3,
            Dead = 4
        }
    }
}