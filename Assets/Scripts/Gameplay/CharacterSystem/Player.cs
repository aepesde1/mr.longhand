﻿using System;
using System.Collections;
using Gameplay.LevelSystem;
using Obi;
using UI;
using UnityEngine;

namespace Gameplay.CharacterSystem
{
    public class Player : MonoBehaviour
    {
        ObiRope[] _ropes;
        ObiParticleAttachment[] _attachments;

        [SerializeField] private CharacterHand leftHand, rightHand, leftLeg, rightLeg;

        public Transform Head;

        [SerializeField] private float FunLimit, SadLimit;

        [SerializeField] private Rigidbody[] _AllRig;


        void Awake()
        {
            _attachments = GetComponentsInChildren<ObiParticleAttachment>();
            foreach (var attachment in _attachments)
            {
                attachment.onBreak += OnBreak;
                //attachment.breakThreshold = 5000;
            }
        }

        public float AllBreakThreshhold;

        public float colorValue;

        void CheckAllBreakThreshold()
        {
            if (GameplayController._instance._State != GameplayController.GameState.Play) return;
            AllBreakThreshhold = 0;
            for (int i = 0; i < _attachments.Length; i++)
            {
                AllBreakThreshhold += _attachments[i].CurrentBreakThreshold;
            }

            if (AllBreakThreshhold < FunLimit)
            {
                HeadChecking._instance.CurrentEmotion = HeadChecking.EmotionFace.Fun;
            }
            else if (AllBreakThreshhold < SadLimit)
            {
                HeadChecking._instance.CurrentEmotion = HeadChecking.EmotionFace.Sad;
            }
            else
            {
                HeadChecking._instance.CurrentEmotion = HeadChecking.EmotionFace.Worry;
            }

            colorValue = AllBreakThreshhold / 17500;
            Color newColor = Color.Lerp(Color.green, Color.red, colorValue);
            Face.material.color = newColor;
        }

        [SerializeField] private Renderer Face;

        private void LateUpdate()
        {
            CheckAllBreakThreshold();
        }

        private void Start()
        {
            StartCoroutine(corouSpawn());
            StartCoroutine(corouEnableRig());
            CameraFollow cameraFollow = FindObjectOfType<CameraFollow>();
            cameraFollow._Playerhead = Head.transform;
        }

        IEnumerator corouEnableRig()
        {
            yield return new WaitForSeconds(0.3f);
            foreach (var r in _AllRig)
            {
                r.isKinematic = false;
            }
        }

        IEnumerator corouSpawn()
        {
            yield return new WaitForSeconds(0.5f);
            foreach (var attachment in _attachments)
            {
                attachment.breakThreshold = 3500;
            }
        }

        public void InitPlayer(SpawnPoints points)
        {
            transform.position = points.player.position;
            StartCoroutine(delaySnapPoint(points));
        }

        IEnumerator delaySnapPoint(SpawnPoints points)
        {
            yield return new WaitForSeconds(0.2f);
            leftHand.SnapToPoint(points.leftHand.position);
            rightHand.SnapToPoint(points.rightHand.position);
            leftLeg.SnapToPoint(points.leftLeg.position);
            rightLeg.SnapToPoint(points.rightLeg.position);
        }

        public void OnBreak()
        {
            if (GameplayController._instance._State != GameplayController.GameState.Play) return;

            HeadChecking._instance.CurrentEmotion = HeadChecking.EmotionFace.Dead;

            Mainmenu._instance.Lose();
        }
    }
}