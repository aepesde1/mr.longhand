using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Gameplay.ComponentsSystem
{
    public class ActionObj : MonoBehaviour
    {
        [SerializeField] private GameObject[] _objtoAction;

        public void Active()
        {
            foreach (var o in _objtoAction)
            {
                o.GetComponent<DOTweenAnimation>().DOPlay();
            }
        }
    }
}