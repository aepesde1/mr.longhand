using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.ComponentsSystem
{
    public class BasketController : MonoBehaviour
    {
        [SerializeField] private GameObject vfxBasket;

        [SerializeField] private bool _isActived;

        [SerializeField] private ActionObj _actionobj;

        void inActive()
        {
            if (_isActived) return;
            vfxBasket.SetActive(true);
            _isActived = true;
            _actionobj.Active();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag.Equals("Grabbable"))
            {
                inActive();
            }
        }
    }
}