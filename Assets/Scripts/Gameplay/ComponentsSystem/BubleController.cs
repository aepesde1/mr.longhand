using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.ComponentsSystem
{
    public class BubleController : MonoBehaviour
    {
        [SerializeField] private GameObject _obj;
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag.Equals("Destroyer"))
            {
                this.gameObject.SetActive(false);
                if (_obj != null)
                {
                    _obj.GetComponent<Collider>().enabled = true;
                    _obj.GetComponent<Rigidbody>().isKinematic = false;
                }
            }
        }
    }
}
