using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Gameplay.ComponentsSystem
{
    public class ButtonStick : MonoBehaviour
    {
        [SerializeField] private ActionObj _actionObj;

        private Animator _anim;

        // Start is called before the first frame update
        void Start()
        {
            _anim = this.GetComponent<Animator>();
        }
        
        public void ButtonActive()
        {
            _anim.Play("ButtonStickActive");
            
            if (_actionObj)
            {
                _actionObj.Active();
            }
        }
    }
}