using System.Collections;
using System.Collections.Generic;
using Gameplay.ComponentsSystem;
using UnityEngine;

public class ButtonSwitch : MonoBehaviour
{
    [SerializeField] private ButtonStick _btnStick;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("BodyMember"))
        {
            _btnStick.ButtonActive();
        }
    }
}
