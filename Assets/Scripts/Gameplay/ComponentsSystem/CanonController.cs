using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace Gameplay.ComponentsSystem
{
    public class CanonController : MonoBehaviour
    {
        [SerializeField] private Animator _animator;

        [SerializeField] private GameObject _CurrentObjectToPush;

        [SerializeField] private int Force;

        [SerializeField] private bool _isLaunching;

        IEnumerator CanonLaunch()
        {
            _isLaunching = true;
            _animator.SetTrigger("loading");
            yield return new WaitForSeconds(2f);
            _animator.SetTrigger("launch");
            yield return new WaitForSeconds(0.4f);
            Vector3 forceDirection = _CurrentObjectToPush.transform.position - this.transform.position;
            _CurrentObjectToPush.GetComponent<Rigidbody>().AddForce(forceDirection * Force, ForceMode.Impulse);
            _isLaunching = false;
            if (_CurrentObjectToPush.gameObject.tag.Equals("CanonBomb"))
            {
                _CurrentObjectToPush.gameObject.GetComponent<Bomb>()._BombFire = true;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag.Equals("Grabbable") || other.gameObject.tag.Equals("CanonBomb"))
            {
                if (!_isLaunching)
                {
                    _CurrentObjectToPush = other.gameObject;
                    StartCoroutine(CanonLaunch());
                }
            }
        }
    }
}