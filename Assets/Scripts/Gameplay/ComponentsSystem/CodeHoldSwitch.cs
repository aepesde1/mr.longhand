using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.ComponentsSystem
{
    public class CodeHoldSwitch : MonoBehaviour
    {
        [SerializeField] private GameObject CubeOff, CubeOn;

        [SerializeField] private GameObject Switch;

        [SerializeField] private Vector3 OrgiginalPosSwitch;

        [SerializeField] private Vector3 TargetPosSwitch;

        [SerializeField] private LevelCodeLogicCheck _levelCodeLogicCheck;

        public bool _RightSwitch;

        void Stay()
        {
            Switch.transform.localPosition = TargetPosSwitch;
            CubeOn.SetActive(true);
            CubeOff.SetActive(false);
            if (!_levelCodeLogicCheck.ListHoldSwitch.Contains(this))
            {
                _levelCodeLogicCheck.ListHoldSwitch.Add(this);
                _levelCodeLogicCheck.CheckList();
            }
        }

        void Exit()
        {
            Switch.transform.localPosition = OrgiginalPosSwitch;
            CubeOn.SetActive(false);
            CubeOff.SetActive(true);
            if (_levelCodeLogicCheck.ListHoldSwitch.Contains(this))
            {
                _levelCodeLogicCheck.ListHoldSwitch.Remove(this);
                _levelCodeLogicCheck.CheckList();
            }
        }

        private void OnTriggerStay(Collider other)
        {
            Stay();
        }

        private void OnTriggerExit(Collider other)
        {
            Exit();
        }
    }
}