using System;
using System.Collections;
using System.Collections.Generic;
using DG.DemiLib;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Gameplay.ComponentsSystem
{
    public class DestroyWall : MonoBehaviour
    {
        [SerializeField] private GameObject[] FractureObj;

        void WallBreak()
        {
            foreach (var o in FractureObj)
            {
                Rigidbody Orig = o.GetComponent<Rigidbody>();
                Orig.isKinematic = false;
                Orig.AddForce(
                    new Vector3(Random.Range(-1, 2), Random.Range(-1, 2), 0) * Random.Range(5, 10),
                    ForceMode.Impulse);
            }

            this.GetComponent<Collider>().enabled = false;
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.tag.Equals("Destroyer") || other.gameObject.tag.Equals("CanonBomb"))
            {
                if (other.gameObject.tag.Equals("CanonBomb"))
                {
                    if (!other.gameObject.GetComponent<Bomb>()._BombFire) return;
                    Destroy(other.gameObject);
                }

                WallBreak();
            }
        }
    }
}