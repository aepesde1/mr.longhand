using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.ComponentsSystem
{
    public class DestroyedItems : MonoBehaviour
    {
        private void OnCollisionEnter(Collision other)
        {
            Destroy(other.gameObject);
        }
    }
}
