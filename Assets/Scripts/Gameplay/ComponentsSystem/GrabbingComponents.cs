using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using Gameplay.CharacterSystem;

namespace Gameplay.ComponentsSystem
{
    public class GrabbingComponents : MonoBehaviour
    {
        private PositionConstraint _positionConstraint;

        private CharacterHand _hand;

        private Rigidbody _rigidbody;

        [SerializeField] private WeaponGrabbing _weapon;

        public bool _isGrabbed;

        [SerializeField] private Transform _grabbingTransform;

        void Start()
        {
            _positionConstraint = this.GetComponent<PositionConstraint>();
            _rigidbody = this.GetComponent<Rigidbody>();
        }

        private void Update()
        {
            if (_isGrabbed && _grabbingTransform != null)
            {
               // if (other.gameObject.transform != _grabbingTransform) return;
                _hand = _grabbingTransform.gameObject.GetComponent<CharacterHand>();
                if (!_hand.isHolding)
                {
                    Ungrabbing();
                }
            }
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.tag.Equals("BodyMember"))
            {
                _hand = other.gameObject.GetComponent<CharacterHand>();
                if (_hand.isHolding && !_isGrabbed)
                {
                    _isGrabbed = true;
                    _positionConstraint.enabled = true;
                    _positionConstraint.SetSource(0,
                        new ConstraintSource { sourceTransform = _hand.transform, weight = 1 });

                    if (_weapon != null)
                    {
                        _weapon.ActiveLookAt();
                    }

                    if (_grabbingTransform == null)
                    {
                        _grabbingTransform = _hand.transform;
                    }
                }
            }
        }

        void Ungrabbing()
        {
            this._rigidbody.isKinematic = true;
            _positionConstraint.constraintActive = false;
            _positionConstraint.enabled = false;

            if (_weapon != null)
            {
                _weapon.inActiveLookAt();
            }

            _isGrabbed = false;
            _grabbingTransform = null;
            Invoke("DelayUngrabbing", 0.1f);
        }

        void DelayUngrabbing()
        {
            this._rigidbody.isKinematic = false;
            _positionConstraint.SetSource(0,
                new ConstraintSource { sourceTransform = null, weight = 1 });
            _positionConstraint.constraintActive = true;
        }
    }
}