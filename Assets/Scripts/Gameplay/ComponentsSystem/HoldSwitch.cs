using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Gameplay.ComponentsSystem
{
    public class HoldSwitch : MonoBehaviour
    {
        [SerializeField] private GameObject CubeOff, CubeOn;

        [SerializeField] private GameObject Switch;

        [SerializeField] private Vector3 OrgiginalPosSwitch;

        [SerializeField] private Vector3 TargetPosSwitch;

        [SerializeField] private bool isSwitchOn;

        [SerializeField] private Transform objT;

        [SerializeField] private float OriginalTrans, TargetTrans;

        [SerializeField] private bool _moveY;

        // Start is called before the first frame update
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                Stay();
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                Exit();
            }
            if (isSwitchOn)
            {
                if (_moveY)
                    objT.DOMoveY(TargetTrans, 2f);
                else
                    objT.DOMoveX(TargetTrans, 2f);
            }
            else
            {
                if (_moveY)
                    objT.DOMoveY(OriginalTrans, 2f);
                else
                    objT.DOMoveX(OriginalTrans, 2f);
            }
        }

        void Stay()
        {
            Switch.transform.localPosition = TargetPosSwitch;
            CubeOn.SetActive(true);
            CubeOff.SetActive(false);
            isSwitchOn = true;
        }

        void Exit()
        {
            Switch.transform.localPosition = OrgiginalPosSwitch;
            CubeOn.SetActive(false);
            CubeOff.SetActive(true);
            isSwitchOn = false;
        }

        private void OnTriggerStay(Collider other)
        {
            Stay();
        }

        private void OnTriggerExit(Collider other)
        {
            Exit();
        }
    }
}