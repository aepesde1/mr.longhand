using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Gameplay.ComponentsSystem
{
    public class KeyLockController : MonoBehaviour
    {
        // Start is called before the first frame update
        [SerializeField] private Animator _animator;

        [SerializeField] private DOTweenAnimation _Tween;

        [SerializeField] private int _LockID;

        void Active()
        {
            _animator.Play("Unlock");
            _Tween.DOPlay();
            this.GetComponent<Collider>().enabled = false;
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.tag.Equals("Grabbable"))
            {
                if (other.gameObject.GetComponent<KeyController>() == null) return;
                if (_LockID != other.gameObject.GetComponent<KeyController>()._keyID) return;

                Destroy(other.gameObject);
                Active();
            }
        }
    }
}