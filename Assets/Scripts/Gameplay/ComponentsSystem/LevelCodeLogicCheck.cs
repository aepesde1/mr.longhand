using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace Gameplay.ComponentsSystem
{
    public class LevelCodeLogicCheck : MonoBehaviour
    {
        public List<CodeHoldSwitch> ListHoldSwitch = new List<CodeHoldSwitch>();

        [SerializeField] int TrueSwitchCount = 0;

        [SerializeField] private Transform objT;

        [SerializeField] private bool _moveY;

        [SerializeField] private float OriginalTrans, TargetTrans;

        [SerializeField] private bool _isDone;

        public void CheckList()
        {
            if (ListHoldSwitch.Count != 3)
            {
                TrueSwitchCount = 0;
                _isDone = false;
            }
            else
            {
                for (int i = 0; i < ListHoldSwitch.Count; i++)
                {
                    if (!ListHoldSwitch[i]._RightSwitch)
                    {
                        TrueSwitchCount = 0;
                        _isDone = false;
                        return;
                    }
                    else
                    {
                        TrueSwitchCount++;
                    }
                }

                if (TrueSwitchCount == 3)
                {
                    _isDone = true;
                    Debug.Log("keolen");
                }
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.C))
            {
                _isDone = true;
            }
            if (Input.GetKeyDown(KeyCode.V))
            {
                _isDone = false;
            }
            
            if (_isDone)
            {
                if (_moveY)
                    objT.DOMoveY(TargetTrans, 10f);
                else
                    objT.DOMoveX(TargetTrans, 10f);
            }
            else
            {
                if (_moveY)
                    objT.DOMoveY(OriginalTrans, 10f);
                else
                    objT.DOMoveX(OriginalTrans, 10f);
            }
        }
    }
}