using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.ComponentsSystem
{
    public class RotateSaw : MonoBehaviour
    {
        [SerializeField] private Vector3 RotateVector;
        // Update is called once per frame
        void Update()
        {
            this.transform.Rotate(RotateVector);
        }
    }
}