using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawDestroy : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Obstacle"))
        {
            this.gameObject.SetActive(false);
        }
    }
}
