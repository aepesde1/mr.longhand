using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.ComponentsSystem
{
    public class SpawnerItems : MonoBehaviour
    {
        [SerializeField] private GameObject _ItemtoSpawn;

        // Start is called before the first frame update
        void Start()
        {
            StartCoroutine(SpawnItem());
        }

        IEnumerator SpawnItem()
        {
            GameObject newobj = Instantiate(_ItemtoSpawn, this.transform.position, Quaternion.identity);
            newobj.SetActive(true);
            yield return new WaitForSeconds(5f);
            StartCoroutine(SpawnItem());
        }
    }
}