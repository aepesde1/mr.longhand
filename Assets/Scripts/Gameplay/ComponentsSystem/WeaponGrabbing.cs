using System.Collections;
using System.Collections.Generic;
using Gameplay.CharacterSystem;
using UnityEngine;
using UnityEngine.Animations;

namespace Gameplay.ComponentsSystem
{
    public class WeaponGrabbing : MonoBehaviour
    {
        // Start is called before the first frame update
        private LookAtConstraint _lookAtConstraint;

        private Transform _HeadTarget;

        [SerializeField] private Collider[] _colliders;

        void Start()
        {
            _lookAtConstraint = this.GetComponent<LookAtConstraint>();
        }

        // Update is called once per frame
        void Update()
        {
        }

        public void ActiveLookAt()
        {
            _HeadTarget = FindObjectOfType<Player>().Head;
            _lookAtConstraint.SetSource(0,
                new ConstraintSource { sourceTransform = _HeadTarget.transform, weight = 1 });
            _lookAtConstraint.enabled = true;
            foreach (var col in _colliders)
            {
                col.isTrigger = true;
            }
        }

        public void inActiveLookAt()
        {
            _lookAtConstraint.SetSource(0,
                new ConstraintSource { sourceTransform = null, weight = 1 });
            _lookAtConstraint.enabled = false;
            foreach (var col in _colliders)
            {
                col.isTrigger = false;
            }
        }
    }
}