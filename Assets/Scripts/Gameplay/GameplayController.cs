﻿using System;
using System.Collections;
using Gameplay.CharacterSystem;
using Gameplay.LevelSystem;
using UI;
using UnityEngine;
using UnityEngine.Serialization;

namespace Gameplay
{
    public class GameplayController : MonoBehaviour
    {
        public static GameplayController _instance;

        public Level currentLevel;

        public Player playerPrefab;

        public Player player;

        public GameState _State;

        [SerializeField] private GameObject _LoadingMask;

        private void Awake()
        {
            if (_instance != null) Destroy(this.gameObject);
            else _instance = this;
        }

        void Start()
        {
            CreateLevel();
        }

        public void CreateLevel()
        {
            if (currentLevel != null) Destroy(currentLevel.gameObject);
            if (player != null) Destroy(player.gameObject);
            StartCoroutine(corouSpawnLevel());
        }

        IEnumerator corouSpawnLevel()
        {
            _LoadingMask.SetActive(true);
            _LoadingMask.GetComponent<Animator>().Play("LoadingScene");
            yield return new WaitForSeconds(0.3f);
            currentLevel = Instantiate(Resources.Load<Level>("Level/" + PersistanceData._CurLevel.ToString()));
            SpawnPlayer();
            yield return new WaitForSeconds(0.7f);
            _LoadingMask.SetActive(false);
        }

        void SpawnPlayer()
        {
            player = Instantiate(playerPrefab, currentLevel.spawnPoints.player.transform.position, Quaternion.identity);
            player.InitPlayer(currentLevel.spawnPoints);
        }

        public void btnNextLevel()
        {
            PersistanceData._CurLevel++;
            if (PersistanceData._CurLevel == 29)
            {
                PersistanceData._CurLevel = 0;
            }

            CreateLevel();
            Mainmenu._instance.TaptoStart();
        }

        public void btnReplay()
        {
            CreateLevel();
            Mainmenu._instance.TaptoStart();
        }

        public enum GameState
        {
            Begining = 1,
            Play = 2,
            End = 3,
        }
    }
}