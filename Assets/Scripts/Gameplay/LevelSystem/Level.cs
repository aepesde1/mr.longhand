﻿using System;
using UnityEngine;

namespace Gameplay.LevelSystem
{
    public class Level : MonoBehaviour
    {
        public SpawnPoints spawnPoints;

        [SerializeField] private bool _isCameraFollow;

        [SerializeField] private Vector3 _OriginalPosCamera;

        [SerializeField] private CameraFollow _camera;

        [SerializeField] private int CameraSize;

        private void Start()
        {
            _camera = FindObjectOfType<CameraFollow>();
            _camera.BeginSetup(_isCameraFollow, _OriginalPosCamera, CameraSize);
        }
    }
}