﻿using UnityEngine;

namespace Gameplay.LevelSystem
{
    public class SpawnPoints : MonoBehaviour
    {
        public Transform player;
        public Transform leftHand, rightHand, leftLeg, rightLeg;
    }
}