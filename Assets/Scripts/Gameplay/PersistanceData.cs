using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public static class PersistanceData
    {
        public static int _CurLevel
        {
            get { return PlayerPrefs.GetInt("_CurLevel", 0); }
            set { PlayerPrefs.SetInt("_CurLevel", value); }
        }
    }
}