﻿using lib.ndk;
using UnityEngine;

namespace Gameplay
{
    public class PlaneXY : sSingleton<PlaneXY>
    {
        [SerializeField] private LayerMask planeLayerMask;

        public Vector3 GetCurrentPosInPlane()
        {
            if (Camera.main != null)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                var cols = Physics.RaycastAll(ray, 1000);


                if (Physics.Raycast(ray, out var hitInfo, 1000, planeLayerMask))
                {
                    return hitInfo.point;
                }
            }

            return Vector3.zero;
        }
    }
}