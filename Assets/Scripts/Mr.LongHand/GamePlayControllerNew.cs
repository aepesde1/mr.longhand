using Gameplay;
using Gameplay.CharacterSystem;
using Gameplay.LevelSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class GamePlayControllerNew : MonoBehaviour
{
    public PlayerNew player;
    public SpawnPoints spawnPoints;

    void Start()
    {
        CreateLevel();
    }
    public void CreateLevel()
    {
        StartCoroutine(corouSpawnLevel());
    }
    IEnumerator corouSpawnLevel()
    {
        yield return new WaitForSeconds(0.3f);
        SpawnPlayer();
        yield return new WaitForSeconds(0.7f);
    }
    void SpawnPlayer()
    {
        player.InitPlayer(spawnPoints);
    }
}
