using System;
using System.Collections;
using System.Collections.Generic;
using Gameplay;
using UnityEngine;

namespace UI
{
    public class Mainmenu : MonoBehaviour
    {
        [SerializeField] private GameObject MenuPanel;

        [SerializeField] private GameObject IngamePanel;

        [SerializeField] private GameObject VictoryPanel;

        [SerializeField] private GameObject LosingPanel;

        public static Mainmenu _instance;

        private void Awake()
        {
            if (_instance != null) Destroy(this.gameObject);
            else _instance = this;
        }

        // Start is called before the first frame update
        public void TaptoStart()
        {
            ActivePanel(IngamePanel.name);
            GameplayController._instance._State = GameplayController.GameState.Play;
        }

        public void ActivePanel(string activepanel)
        {
            MenuPanel.SetActive(activepanel.Equals(MenuPanel.name));
            IngamePanel.SetActive(activepanel.Equals(IngamePanel.name));
            VictoryPanel.SetActive(activepanel.Equals(VictoryPanel.name));
            LosingPanel.SetActive(activepanel.Equals(LosingPanel.name));
        }

        public void GoHome()
        {
            ActivePanel(MenuPanel.name);
            GameplayController._instance._State = GameplayController.GameState.Begining;
            GameplayController._instance.CreateLevel();
        }

        public void Victory()
        {
            ActivePanel(VictoryPanel.name);
            GameplayController._instance._State = GameplayController.GameState.End;
        }

        public void Lose()
        {
            ActivePanel(LosingPanel.name);
            GameplayController._instance._State = GameplayController.GameState.End;
        }
    }
}