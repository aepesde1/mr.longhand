using System;
using System.Collections;
using System.Collections.Generic;
using Gameplay;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UIGameplay : MonoBehaviour
    {
        [SerializeField] private Text _LevelText;

        [SerializeField] private Image _btnSound, _btnVibrate;

        [SerializeField] private Sprite SoundOn, SoundOff;

        [SerializeField] private Sprite VibrateOn, VibrateOff;

        [SerializeField] private GameObject GroupSetting;

        private void OnEnable()
        {
            _LevelText.text = "Level " + (PersistanceData._CurLevel + 1).ToString();
            _btnSound.sprite = SoundCheck == 1 ? SoundOn : SoundOff;
            _btnVibrate.sprite = VibrateCheck == 1 ? VibrateOn : VibrateOff;
        }

        public static int SoundCheck
        {
            get { return PlayerPrefs.GetInt("SoundCheck", 1); }
            set { PlayerPrefs.SetInt("SoundCheck", value); }
        }

        public static int VibrateCheck
        {
            get { return PlayerPrefs.GetInt("VibrateCheck", 1); }
            set { PlayerPrefs.SetInt("VibrateCheck", value); }
        }

        public void SoundChange()
        {
            SoundCheck = SoundCheck == 1 ? 0 : 1;
            _btnSound.sprite = SoundCheck == 1 ? SoundOn : SoundOff;
            Vibration.VibratePop();
        }

        public void VibrateChange()
        {
            VibrateCheck = VibrateCheck == 1 ? 0 : 1;
            _btnVibrate.sprite = VibrateCheck == 1 ? VibrateOn : VibrateOff;
            Vibration.VibratePop();
        }

        public void BtnSetting()
        {
            GroupSetting.SetActive(!GroupSetting.activeSelf);
        }
    }
}