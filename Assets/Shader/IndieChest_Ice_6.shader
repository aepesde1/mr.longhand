Shader "IndieChest/Ice" {
	Properties {
		[Header(Translucency)] _Translucency ("Strength", Range(0, 50)) = 1
		_TransNormalDistortion ("Normal Distortion", Range(0, 1)) = 0.1
		_TransScattering ("Scaterring Falloff", Range(1, 50)) = 2
		_TransDirect ("Direct", Range(0, 1)) = 1
		_TransAmbient ("Ambient", Range(0, 1)) = 0.2
		_TransShadow ("Shadow", Range(0, 1)) = 0.9
		_DeepCcolor ("Deep Ccolor", Vector) = (1,1,1,0)
		_Surfacecolor ("Surface color", Vector) = (0.3396226,0.3396226,0.3396226,0)
		_MainTexture ("MainTexture", 2D) = "white" {}
		[Normal] _Noise01Bump ("Noise01Bump", 2D) = "white" {}
		_Icescratches ("Ice scratches", 2D) = "white" {}
		_Noise1 ("Noise1", 2D) = "white" {}
		[HideInInspector] _texcoord ("", 2D) = "white" {}
		[HideInInspector] __dirty ("", Float) = 1
	}
	//DummyShaderTextExporter
	SubShader{
		Tags { "RenderType" = "Opaque" }
		LOD 200
		CGPROGRAM
#pragma surface surf Standard
#pragma target 3.0

		struct Input
		{
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			o.Albedo = 1;
		}
		ENDCG
	}
	Fallback "Diffuse"
}