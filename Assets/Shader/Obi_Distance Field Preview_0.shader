Shader "Obi/Distance Field Preview" {
	Properties {
		_Volume ("Texture", 3D) = "" {}
		_AABBMin ("AABB Min", Vector) = (-0.5,-0.5,-0.5,1)
		_AABBMax ("AABB Max", Vector) = (0.5,0.5,0.5,1)
		_InsideColor ("Inside color", Vector) = (1,1,1,1)
		_OutsideColor ("Outside color", Vector) = (0,0,0,1)
		_Absorption ("Absorption", Float) = 1.5
		_StepSize ("Step size", Float) = 0.01
		_MaxSteps ("Max steps", Float) = 300
	}
	//DummyShaderTextExporter
	SubShader{
		Tags { "RenderType" = "Opaque" }
		LOD 200
		CGPROGRAM
#pragma surface surf Standard
#pragma target 3.0

		struct Input
		{
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			o.Albedo = 1;
		}
		ENDCG
	}
	Fallback "VertexLit"
}