using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Tarodev
{
    public class EnterPlayMode : EditorWindow
    {
        [MenuItem("Window/Play From Menu")]
        static void PlayFromMenu()
        {
            if (!EditorApplication.isPlaying)
            {
                //EditorApplication.ExecuteMenuItem("Edit/Play");
                //EditorApplication.ExecuteMenuItem("File/OpenRecentScene/Gameplay");
                //EditorApplication.ExecuteMenuItem("Edit/Play");

                EditorSceneManager.SaveOpenScenes();
                EditorSceneManager.OpenScene($"Assets/_Project/Scene/Menu.unity");
                EditorApplication.EnterPlaymode();
            }
        }
        
        
    }
}