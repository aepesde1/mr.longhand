using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Tarodev
{
    public class LoadSpecificSceneInEditor : EditorWindow
    {
        private string sceneToLoadName = "Menu";

        [MenuItem("Window/Load Scene")]
        static void Init()
        {
            LoadSpecificSceneInEditor window =
                (LoadSpecificSceneInEditor) EditorWindow.GetWindow(typeof(LoadSpecificSceneInEditor));
            window.Show();
        }

        void OnGUI()
        {
            GUILayout.Label("Load Scene", EditorStyles.boldLabel);
            sceneToLoadName = EditorGUILayout.TextField("Scene Name", sceneToLoadName);

            if (GUILayout.Button("Load"))
            {
                // Save the current scene before loading a new one
                EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();

                // Load the new scene
                Scene scene = EditorSceneManager.OpenScene("Assets/_Project/Scene/" + sceneToLoadName + ".unity",
                    OpenSceneMode.Single);
            }
        }
    }
}