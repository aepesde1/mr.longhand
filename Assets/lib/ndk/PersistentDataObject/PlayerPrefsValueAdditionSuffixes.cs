﻿using UnityEngine;

namespace lib.ndk.PersistentDataObject
{
    public class PlayerPrefsValueAdditionSuffixes<T>
    {
        private string key;
        private T defaultValue;

        public PlayerPrefsValueAdditionSuffixes(string key, T defaultValue)
        {
            this.key = key;
            this.defaultValue = defaultValue;
        }

        string GetPrefKey(string suffixes)
        {
            return key + "_" + suffixes;
        }

        public T GetValue(string suffixes)
        {
            if (PlayerPrefs.HasKey(GetPrefKey(suffixes)))
            {
                string serializedValue = PlayerPrefs.GetString(GetPrefKey(suffixes));
                return JsonUtility.FromJson<T>(serializedValue);
            }
            else
            {
                return defaultValue;
            }
        }

        public void SetValue(string suffixes, int value)
        {
            string serializedValue = JsonUtility.ToJson(value);
            PlayerPrefs.SetString(GetPrefKey(suffixes), serializedValue);
        }
    }
}