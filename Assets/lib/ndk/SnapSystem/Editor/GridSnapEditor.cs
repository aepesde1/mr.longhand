﻿using UnityEditor;
using UnityEngine;

namespace lib.ndk.SnapSystem.Editor
{
    [CustomEditor(typeof(GridSnap))]
    public class GridSnapEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (Application.isPlaying) return;
            GridSnap gridSnap = (GridSnap)target;
            if (gridSnap != null)
            {
                gridSnap.SnapToGrid();
            }
        }
    }
}