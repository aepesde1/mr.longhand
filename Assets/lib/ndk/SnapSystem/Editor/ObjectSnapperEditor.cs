﻿using UnityEditor;
using UnityEngine;

namespace lib.ndk.SnapSystem.Editor
{
    [CustomEditor(typeof(ObjectSnapper))]
    public class ObjectSnapperEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {

            base.OnInspectorGUI();

            if (Application.isPlaying) return;
            ObjectSnapper snapper = (ObjectSnapper)target;
            snapper.TrySnapToTarget();

            if (GUILayout.Button("Snap to Grid"))
            {
            }
        }
    }
}