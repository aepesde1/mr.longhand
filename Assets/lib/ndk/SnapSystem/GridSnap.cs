﻿using UnityEngine;

namespace lib.ndk.SnapSystem
{
    public class GridSnap : MonoBehaviour
    {
        public float gridSize = 1.0f;

        public void SnapToGrid()
        {
            // Get the current position of the GameObject
            Vector3 currentPos = transform.position;

            // Snap the position to the nearest grid point
            float snappedX = Mathf.Round(currentPos.x / gridSize) * gridSize;
            float snappedY = Mathf.Round(currentPos.y / gridSize) * gridSize;
            float snappedZ = Mathf.Round(currentPos.z / gridSize) * gridSize;

            // Set the new snapped position
            transform.position = new Vector3(snappedX, snappedY, snappedZ);
        }
    }
}