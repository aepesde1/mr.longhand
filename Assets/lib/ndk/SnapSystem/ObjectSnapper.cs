﻿using UnityEngine;

namespace lib.ndk.SnapSystem
{
    public class ObjectSnapper : MonoBehaviour
    {
        public float snapRadius = 0.1f; // The distance to snap objects.
        public LayerMask snapLayer; // The layer that objects should snap to.

        protected GameObject gojToSnap;
        
        public virtual void TrySnapToTarget()
        {
            gojToSnap = null;
            var col = Physics2D.OverlapCircle(transform.position, snapRadius, snapLayer);
            if (col != null)
            {
                gojToSnap = col.gameObject;
                transform.position = col.transform.position;
            }
        }
    }
}