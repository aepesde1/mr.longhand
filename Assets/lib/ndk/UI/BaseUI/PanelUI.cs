﻿using System;
using lib.ndk.UI.Tween;
using Sirenix.OdinInspector;
using UnityEngine;

namespace lib.ndk.UI.BaseUI
{
    //[ExecuteInEditMode]
    public class PanelUI : MonoBehaviour
    {
        [ReadOnly] [SerializeField] protected GameObject panelGroup;
        [ReadOnly] [SerializeField] protected UITweenManager2 tmShow, tmHide;
        [ReadOnly] [ShowInInspector] public bool IsShow { get; private set; } = false;

        const string SHOW_TWEEN_ID = "show";
        const string HIDE_TWEEN_ID = "hide";

        protected void OnValidate()
        {
            InitTween();
        }

        protected virtual void Awake()
        {
            InitTween();
        }

        [Button]
        protected void InitTween()
        {
            //Debug.Log("Init Tween");
            panelGroup = transform.GetChild(0).gameObject;
            tmShow = new UITweenManager2();
            tmHide = new UITweenManager2();
            tmShow.InitTween(gameObject, SHOW_TWEEN_ID);
            tmHide.InitTween(gameObject, HIDE_TWEEN_ID);
        }

        [Button]
        public virtual void SShow()
        {
            if (IsShow) return;
            Debug.Log("Show: " + name);
            Debug.Log(Time.time);
            EnablePanel();
            IsShow = true;
            tmHide.StopAllTween();
            tmShow.Play(OnShowDone);
        }


        [Button]
        public virtual void HHide()
        {
            if (!IsShow) return;
            Debug.Log("Hide: " + name);

            IsShow = false;
            tmShow.StopAllTween();
            tmHide.Play(OnHideDone);
        }

        public virtual void OnShowDone()
        {
            onShowDone?.Invoke();
            Debug.Log("Show Done: " + name);
        }

        public virtual void OnHideDone()
        {
            onHideDone?.Invoke();
            DisablePanel();
            Debug.Log("Hide Done: " + name);
            //DisablePanel();
        }


        public void EnablePanel()
        {
            panelGroup.SetActive(true);
        }

        public void DisablePanel()
        {
            panelGroup.SetActive(false);
        }

        public Action onShowDone;
        public Action onHideDone;
    }
}