using System;
using lib.ndk.Tool;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace lib.ndk.UI.Button
{
    public class UIButton : MonoBehaviour, IPointerClickHandler
    {
        protected virtual void OnClick()
        {
            //if (!this.CanClick()) return;
            onClick?.Invoke();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!this.CanClick()) return;
            OnClick();
        }

        public UnityEvent onClick;
    }
}