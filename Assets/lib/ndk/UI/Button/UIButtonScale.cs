﻿using System;
using DG.Tweening;
using UnityEngine;

namespace lib.ndk.UI.Button
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(UIButton))]
    public class UIButtonScale : MonoBehaviour
    {
        UIButton _uiButton;
        Animation _animation;

        void Awake()
        {
            _uiButton = GetComponent<UIButton>();
            _animation = GetComponent<Animation>();

            UpdateAnimClip();
            _uiButton.onClick.AddListener(PlayAnimScale);
        }

        void UpdateAnimClip()
        {
            CheckAndAddAnimation();
            if(_animation.clip == null )_animation.clip = Resources.Load<AnimationClip>("ScaleButtonAnimation");
            _animation.playAutomatically = false;
            _animation.hideFlags = HideFlags.HideInInspector;
        }

#if UNITY_EDITOR
        void Update()
        {
            if (Application.isPlaying) return;
            UpdateAnimClip();
        }
#endif

#if UNITY_EDITOR
        void OnDestroy()
        {
            // if (Application.isPlaying) return;
            // if (_animation == null) _animation = GetComponent<Animation>();
            // if (_animation != null) DestroyImmediate(_animation);
        }
#endif

        void CheckAndAddAnimation()
        {
            if (_animation == null)
            {
                _animation = gameObject.AddComponent<Animation>();
            }
        }

        void PlayAnimScale()
        {
            //Debug.Log("Play Anim Scale");
            _animation.Stop();
            _animation.Play();
        }
    }
}