using System;
using UnityEngine;
using UnityEngine.UI;

namespace lib.ndk.UI.Button
{
    public abstract class UIButtonToggle : UIButton
    {
        [SerializeField] protected GameObject activeGroup, deactiveGroup;

        protected void OnEnable()
        {
            UpdateDisplay();
        }

        protected override void OnClick()
        {
            Debug.Log("Click Toggle");
            InverseState();
            UpdateDisplay();
        }

        protected virtual void UpdateDisplay()
        {
            activeGroup.SetActive(GetCurrentState());
            deactiveGroup.SetActive(!GetCurrentState());
        }

        protected abstract void InverseState();

        protected abstract bool GetCurrentState();
    }
}