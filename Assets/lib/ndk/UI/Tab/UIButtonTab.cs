﻿using System;
using lib.ndk.Tool;
using UnityEngine;

namespace lib.ndk.UI.Tab
{
    public class UIButtonTab : MonoBehaviour
    {
        [SerializeField] private GameObject activeButton;
        
        public void ActiveButton(bool isActive)
        {
            activeButton.SetActive(isActive);
        }

        void OnClick()
        {
            if (!this.CanClick()) return;
            onClick?.Invoke(this);
        }

        public Action<UIButtonTab> onClick;
    }
}