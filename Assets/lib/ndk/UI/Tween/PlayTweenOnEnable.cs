﻿// using Sirenix.Utilities;
// using UnityEngine;
//
// namespace ndk.UI.Tween
// {
//     public class PlayTweenOnEnable : MonoBehaviour
//     {
//         [SerializeField] private int tweenGroup;
//
//         private UITweener[] _tweeners;
//
//
//         private void Awake()
//         {
//             _tweeners = GetComponentsInChildren<UITweener>();
//             
//         }
//
//         private void OnEnable()
//         {
//             if (_tweeners.IsNullOrEmpty()) return;
//             foreach (UITweener tweener in _tweeners)
//             {
//                 if (tweener.tweenGroup != tweenGroup) continue;
//                 tweener.PlayForward();
//                 tweener.ResetToBeginning();
//             }
//         }
//     }
// }